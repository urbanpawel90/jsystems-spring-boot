package com.jsystems.springboot.controller;

import com.jsystems.springboot.exception.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionControllerAdvice {
  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public String handleEntityNotFound(EntityNotFoundException ex) {
    return String.format("Entity of type %s with ID=%s doesn't exist!",
        ex.getEntityType(), ex.getId());
  }
}
