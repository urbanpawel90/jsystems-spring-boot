package com.jsystems.springboot.controller;

import com.jsystems.springboot.entity.Employee;
import com.jsystems.springboot.exception.EntityNotFoundException;
import com.jsystems.springboot.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
  private final EmployeeRepository repository;

  public EmployeeController(EmployeeRepository repository) {
    this.repository = repository;
  }

  @GetMapping
  public List<Employee> getAll() {
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Employee findOne(@PathVariable("id") int employeeId) {
    Employee employee = repository.findOne(employeeId);
    if (employee == null) {
      throw new EntityNotFoundException("Employee",
          Integer.toString(employeeId));
    }
    return employee;
  }

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void update(@PathVariable("id") int id,
      @RequestBody Employee employee) {
    Employee existingEmployee =
        Optional.ofNullable(repository.findOne(id))
            .orElseThrow(() -> new EntityNotFoundException(
                "Employee", Integer.toString(id)));

    employee.setId(id);
    if (employee.getDepartment() == null) {
      employee.setDepartment(existingEmployee.getDepartment());
    }
    if (employee.getManager() == null) {
      employee.setManager(existingEmployee.getManager());
    }
    repository.save(employee);
  }
}
